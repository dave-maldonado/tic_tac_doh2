# board class for Tic Tac Toe
# David Maldonado 2013
class Board
  def initialize(board_size=3) # 3 would be 3x3 board, 4 is 4x4, etc
    @board = Array.new(board_size**2)
    @solutions = []
    generate_solutions
  end

  def size
    @board.size
  end
  
  def [](index)
    @board[index]
  end

  def []=(index, mark)
    @board[index] = mark
  end

  def full?
    not @board.include? nil
  end

  def has_won?
    solutions.any? { |solution| solution.uniq.length == 1 and not solution.include? nil }
  end

  # returns array of @board indices for empty squares
  def empty_square_indices
    # maps indices ONLY to the elements representing empty squares
    temp = @board.each_with_index.map { |square, index| index if square == nil } 
    # collects the indices into a new array
    temp.select { |square| square.class == Fixnum }
  end

  def print_3x3_to_console
    puts <<-EOS.gsub(/^ */, '')

    |#{@board[0]}|#{@board[1]}|#{@board[2]}|
    |#{@board[3]}|#{@board[4]}|#{@board[5]}|
    |#{@board[6]}|#{@board[7]}|#{@board[8]}|
    EOS
  end
  
  private

  def generate_solutions
    # UGH
  end
end




require 'minitest/autorun'
require 'minitest/pride'
require 'board'

class BoardTest < MiniTest::Test
  def setup
    @board = Board.new
  end

  def teardown
    @board = nil
  end
  
  def test_board
    refute_nil @board
  end

  def test_sq_bracket
    assert_nil @board[0]
  end

  def test_sq_braket_equals
    @board[0] = :X
    assert_equal @board[0], :X
  end

  def test_full?
    (0..@board.size - 1).each { |n| @board[n] = :X }
    assert_equal @board.full?, true
  end

  def test_empty_square_indices
    @board[0] = :X; @board[1] = :X; @board[2] = :X
    assert_equal @board.empty_square_indices, [3, 4, 5, 6, 7, 8]
  end
end



